﻿using UnityEngine;
using System.Collections;

public class EnemyAI_Second : CubeAi {

	private bool m_alive = true;
	private bool check = true;
	public AudioClip attackSound;
	private bool hasKilled = false;

	private float killmeter = 0;
	public float killDelay = 3f;
	public ParticleSystem bleedSystem;
	public GameObject explodePrefab;

	void Start(){
		player = GameObject.Find("Player").transform;
		currentState = new CubeStateSleep (Random.Range (0, 5f));
		agent = GetComponent<NavMeshAgent> ();
		transform.GetChild (0).renderer.material.color = Color.white;
	}

	void Update () {
		if(m_alive){
			if((!observed)){
				killmeter = Mathf.Clamp(killmeter - Time.deltaTime,0,killDelay+1);
				if (currentState.update (transform)/* && Time.time > lastTime + updateInterval*/){
					int[] weigths = {1,1,3,1,4};
					int rand = weigthedRandom(weigths);
					//Choose a new state
					switch(rand){
					case 0:
						currentState = new CubeStateRotate (Quaternion.LookRotation(player.position - transform.position - Vector3.up*1f), Random.Range(10,30f));
						break;
					case 1:
						currentState = new CubeStateSleep(Random.Range(1f,3f));
						break;
					case 2:
						currentState = new CubeStateMove(getRandPos(transform.position),agent);
						break;
					case 3:
						currentState = new CubeStateRotate(Quaternion.Euler(new Vector3(0,Random.Range (0,360),0)), Random.Range(10,30f));
						break;
					case 4:
						currentState = new CubeStateFollow(player.position, agent);
						break;
					}
					
					
				}
				checkPlayer();
			}else{
				killmeter = Mathf.Clamp(killmeter + Time.deltaTime,0,killDelay+1);
				if(killmeter > killDelay){
					killPlayer();
				}
			}
		}
	}
	
	public void Dead(){
		m_alive = false;
		agent.Stop();
		StartCoroutine("CoDead");
	}
	
	IEnumerator CoDead(){
		float starttime = Time.time;
		float duration = 10;
		player.BroadcastMessage("DisableStuff");
		player.BroadcastMessage("WinGame");

		bleedSystem.Play ();
		yield return new WaitForSeconds(1f);
		Instantiate (explodePrefab, transform.position, Quaternion.identity);
		transform.GetChild (0).renderer.enabled = false;
		transform.collider.enabled = false;
		this.enabled = false;
		
	}
	
	public void overrideState(CubeState state){
		//Debug.Log(currentState+" "+state);
		if(currentState.GetType() != state.GetType()){
			currentState = state;
		}
	}
	
	public void checkPlayer(){
		if(agent != null){
			if(Vector3.Distance(player.position, transform.position) < 5){
				if(check){
					agent.SetDestination (player.position);
					check = false;
				}
				
				if(!agent.pathPending){
					float low = agent.remainingDistance;
					float high = 5f;
					if(low < high){
						if(!hasKilled){
							hasKilled = true;
							killPlayer();
						}
					}else{
						agent.Stop();
					}
				}
			}else if(Vector3.Distance(player.position, transform.position) > 10){
				check = true;
			}
		}
	}
	
	public void killPlayer(){
		Transform child = transform.GetChild(0);
		child.renderer.material.color = new Color(1f, 0f, 0f);
		AudioSource.PlayClipAtPoint (attackSound, transform.position);
		if(agent != null)
			agent.Stop();
		Destroy(transform.GetComponent<NavMeshAgent>());
		collider.enabled = false;
		overrideState(new CubeStateKill(player));
	}
	
	public void ObserveEnter(){
		agent.Stop ();
		observed = true;
	}
	public void ObserveExit(){
		observed = false;
		if(agent != null)
			agent.Resume ();
	}
	
}


class CubeStateKill : CubeState{
	private Transform m_player;
	
	public CubeStateKill(Transform player){
		m_player = player;
	}
	
	public override bool update (Transform enemyCube){
		enemyCube.position = Vector3.Lerp(enemyCube.position, m_player.position, 0.1f);
		m_player.GetComponent<Dead>().playerDead(enemyCube);
		//dead
		return false;
	}
}

public 	class CubeStateFollow : CubeState{
	public Vector3 target;
	NavMeshAgent agent;
	Quaternion targetRotation;
	
	bool isAtDestination(NavMeshAgent agent){
		if(agent != null){	
			if (!agent.pathPending){
				if (!agent.hasPath || agent.velocity.sqrMagnitude <= 0.1f){
					return true;
				}
			}
		}
		return false;
	}
	
	public CubeStateFollow(Vector3 tar,NavMeshAgent _agent){
		target = tar * 0.7f;
		agent = _agent;
		targetRotation = Quaternion.LookRotation ((tar - agent.transform.position - Vector3.up * 1f));
		agent.SetDestination (target);
	}
	
	public override bool update(Transform cube){
		float f = 50 * Time.deltaTime;
		cube.rotation = Quaternion.RotateTowards (cube.rotation, targetRotation, f);
		return isAtDestination(agent);
	}
}


