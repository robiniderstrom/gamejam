﻿using UnityEngine;
using System.Collections;

public class CubeAi : MonoBehaviour {


	public CubeState currentState;
	public float updateInterval = 3f;
	public string state;

	public Transform player;
	private float shakemeter = 0;
	public float shakeDelay = 2f;
	public float jitterOffset = 0.1f;
	public NavMeshAgent agent;
	public bool observed = false;
	// Use this for initialization
	void Start () {
		player = GameObject.Find("Player").transform;
		currentState = new CubeStateSleep (Random.Range (0, 5f));
		agent = GetComponent<NavMeshAgent> ();
	}

	public Vector3 getRandPos(Vector3 startPos){

		Vector3 randPos = startPos + Random.insideUnitSphere*Random.Range (1f,30f);
		randPos.y = startPos.y;
	
		return randPos;
	}

	public int weigthedRandom(int[] weigths){

		int totalSum = 0;
		for(int i= 0; i<weigths.Length;i++){
			totalSum += weigths[i];
		}
		int sum = 0;
		int rand = Random.Range (1, totalSum);
		for(int i= 0; i<weigths.Length;i++){
			sum += weigths[i];
			if(sum >= rand){
				return i;
			}
		}
		Debug.Log ("Something weent wrong");
		return 0;
	}
	
	// Update is called once per frame
	void Update () {
		if (!observed) {
			shakemeter = Mathf.Clamp(shakemeter - Time.deltaTime,0,shakeDelay+1);
			if (currentState.update (transform)/* && Time.time > lastTime + updateInterval*/) {
				int[] weigths = {1,2,5,2};
				int rand = weigthedRandom (weigths);
				//Choose a new state
				switch (rand) {
				case 0:
						state = "RotateToPlayer";
						currentState = new CubeStateRotate (Quaternion.LookRotation (player.position - transform.position - Vector3.up * 1f), Random.Range (10, 30f));
						break;
				case 1:
						state = "sleep";
						currentState = new CubeStateSleep (Random.Range (1f, 3f));
						break;
				case 2:
						state = "randomMove";
						currentState = new CubeStateMove (getRandPos (transform.position), agent);
						break;
				case 3:
						state = "randomRotate";
						currentState = new CubeStateRotate (Quaternion.Euler (new Vector3 (0, Random.Range (0, 360), 0)), Random.Range (10, 30f));
						break;
				}
			}
		}else{
			shakemeter = Mathf.Clamp(shakemeter + Time.deltaTime,0,shakeDelay+1);
			if(shakemeter > shakeDelay){
				transform.GetChild(0).localPosition = Vector3.up * 0.5f + new Vector3 (Random.Range (-jitterOffset, jitterOffset), Random.Range (-jitterOffset, jitterOffset), Random.Range (-jitterOffset, jitterOffset));
			}
		}
	}

	public void ObserveEnter(){
		agent.Stop ();
		observed = true;
	}
	public void ObserveExit(){
		observed = false;
		agent.Resume ();
	}
}


public class CubeState{
	public virtual bool update(Transform cube){return false;}
}

public 	class CubeStateRotate : CubeState{
	public Quaternion targetRotation;
	public float rotationSpeed;
	public CubeStateRotate(Quaternion targetRot,float speed){
		targetRotation = targetRot;
		rotationSpeed = speed;
	}
	public override bool update(Transform cube){
		float f = rotationSpeed * Time.deltaTime;
		cube.rotation = Quaternion.RotateTowards (cube.rotation, targetRotation, f);
		if (Quaternion.Angle (cube.rotation,targetRotation)<8) {
			return true;		
		}
		return false;
	}
}

public 	class CubeStateMove : CubeState{
	public Vector3 target;
	NavMeshAgent agent;
	Quaternion targetRotation;

	bool isAtDestination(NavMeshAgent agent){
		if (!agent.pathPending){
			if (!agent.hasPath || agent.velocity.sqrMagnitude <= 0.1f){
				return true;
			}
		}
		return false;
	}

	public CubeStateMove(Vector3 tar,NavMeshAgent _agent){
		target = tar;
		agent = _agent;
		targetRotation = Quaternion.LookRotation (tar - agent.transform.position - Vector3.up * 1f);
		agent.SetDestination (target);
	}

	public override bool update(Transform cube){
		float f = 50 * Time.deltaTime;
		cube.rotation = Quaternion.RotateTowards (cube.rotation, targetRotation, f);
		return isAtDestination(agent);
	}
}

public 	class CubeStateSleep : CubeState{
	public float duration;
	public float startTime;
	
	public CubeStateSleep(float dur){
		duration = dur;
		startTime = Time.time;
	}
	
	public override bool update(Transform cube){
		if (Time.time > startTime + duration) {
			return true;		
		}
		return false;
	}
}
