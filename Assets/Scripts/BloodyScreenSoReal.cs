﻿using UnityEngine;
using System.Collections;

public class BloodyScreenSoReal : MonoBehaviour {

	public Material mat;
	public float duration = 1f;
	// Use this for initialization
	private Material matCopy;
	void Start(){
		renderer.enabled = false;
		mat.color = Color.white;
	}
	
	public void Bleed(){
		StartCoroutine (bloody());

	}

	IEnumerator bloody(){
		renderer.enabled = true;
		float startTime = Time.time;
		while (startTime + duration > Time.time) {
			mat.color = new Color(1f,0,0,Mathf.LerpAngle(0f,1f,(Time.time-startTime)/duration));		
			yield return null;
		}
	}
}
