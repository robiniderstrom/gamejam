﻿using UnityEngine;
using System.Collections;

public class scarycube : MonoBehaviour {


	private float offset;
	private float freq;
	private float magnitude;
	// Use this for initialization
	void Start () {
		offset = Random.Range (0, 30);
		freq = Random.Range (1, 8);
		magnitude = Random.Range (0.01f, 0.06f);
	}
	
	// Update is called once per frame
	void Update () {
		transform.localScale = Vector3.one + Vector3.one * Mathf.Sin(Time.time*freq + offset) * magnitude;

	}
}
