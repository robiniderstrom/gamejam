﻿using UnityEngine;
using System.Collections;

public class Dead : MonoBehaviour {
	public AudioClip clip;

	bool alive = true;

	public void playerDead(Transform enemyCube){
		if (alive) {
			alive = false;
			StartCoroutine(Die (enemyCube));

			audio.clip = clip;
			audio.Play ();
		}
	}

	IEnumerator Die(Transform enemyCube){
		float startTime = Time.time;
		float duration = 2f;
		transform.LookAt (enemyCube);
		gameObject.GetComponent<CharacterMotor> ().enabled = false;
		gameObject.GetComponent<MouseLook> ().enabled = false;
		Camera.main.GetComponent<MouseLook> ().enabled = false;
		Camera.main.BroadcastMessage("Bleed");
		yield return new WaitForSeconds(duration);
		Application.LoadLevel("MenuScene");
	}
}
