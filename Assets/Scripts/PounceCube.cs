﻿using UnityEngine;
using System.Collections;

public class PounceCube : MonoBehaviour {

	public Transform target;
	public float range = 5;
	public float pounceDelay = 3;
	
	private bool hasPounced = false;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if ((target.position - transform.position).magnitude < range && !hasPounced) {
			hasPounced = true;
			StartCoroutine(PounceRoutine());
		}
	}

	IEnumerator PounceRoutine(){
		float startTime = Time.time;
		Vector3 orgPos = transform.position;
		while (startTime + pounceDelay > Time.time) {
			float dist = 0.03f;
			transform.position = orgPos + new Vector3(Random.Range(-dist,dist),Random.Range(-dist,dist),Random.Range(-dist,dist));
			yield return null;
		}
		startTime += pounceDelay;
		float delay = 0.3f;

		while(startTime + delay > Time.time){

			transform.position = Vector3.Lerp(orgPos,target.position,(Time.time - startTime)*0.33f);
			yield return null;
		}

	}
}
