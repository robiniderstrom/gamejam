﻿using UnityEngine;
using System.Collections;

public class AmbientAudio : MonoBehaviour {

	public AudioClip[] clips;
	public Vector2 delayRange = new Vector2(3,50);
	private float delay;

	private float lastTime = 0;

	private AudioSource source;
	// Use this for initialization
	void Start () {
		source = GetComponent<AudioSource> ();
		delay = Random.Range (delayRange.x, delayRange.y);
	}
	
	// Update is called once per frame
	void Update () {
		if (Time.time > lastTime + delay) {
			lastTime = Time.time;
			delay = Random.Range (delayRange.x, delayRange.y);
			source.clip = clips[Random.Range (0,clips.Length)];
			source.Play();
		}
	}
}
