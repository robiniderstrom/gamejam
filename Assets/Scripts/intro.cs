﻿using UnityEngine;
using System.Collections;

public class intro : MonoBehaviour {

	public MonoBehaviour[] stuffToDisable;
	public Renderer[] stuffToFadeOut;

	public void DisableStuff(){
		foreach (MonoBehaviour m in stuffToDisable) {
			m.enabled = false;		
		}
	}

	// Use this for initialization
	void Start () {
		foreach (MonoBehaviour m in stuffToDisable) {
			m.enabled = false;		
		}

		foreach (Renderer m in stuffToFadeOut) {
			m.material.color = Color.white;	
		}
		StartCoroutine (startUp ());
	}

	IEnumerator startUp(){
		float startTime = Time.time;
		float durationText = 2;
		float durationFade = 0.5f;
		while (startTime + durationText > Time.time) {
			yield return null;
		}
		startTime += durationText;
		while (startTime + durationFade > Time.time) {
			foreach (Renderer m in stuffToFadeOut) {
				m.material.color = new Color(1,1,1,1f-(Time.time - startTime)/durationFade);	
			}
			yield return null;
		}

		foreach (MonoBehaviour m in stuffToDisable) {
			m.enabled = true;		
		}
		foreach (Renderer m in stuffToFadeOut) {
			m.material.color = new Color(1,1,1,0);	
		}
	}
	

}
