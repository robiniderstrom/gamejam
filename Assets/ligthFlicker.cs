﻿using UnityEngine;
using System.Collections;

public class ligthFlicker : MonoBehaviour {

	public AudioClip ligthOff;
	// Use this for initialization
	void Start () {

		StartCoroutine (flicker ());
	}
	
	IEnumerator flicker(){
		while (true) {
			light.intensity = 0.5f;
			yield return new WaitForSeconds(Random.Range (2f,6f));
			audio.clip = ligthOff;
			audio.Play();
			light.intensity = 0.05f;
			yield return new WaitForSeconds(Random.Range (0.3f,1f));
		}
	}
}
