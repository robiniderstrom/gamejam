﻿using UnityEngine;
using System.Collections;

public class PlayButton : MonoBehaviour {

	private bool clicked = false;
	public AudioClip clip;

	void Start(){
		Screen.showCursor = true;
	}

	void OnMouseUpAsButton() {
		if (!clicked) {
			clicked = true;
			audio.clip = clip;
			audio.Play();
			StartCoroutine(deleyedStart());
		}
	}

	void Update(){
		if (Input.GetKeyDown (KeyCode.Escape)) {
			Application.Quit();		
		}
	}

	IEnumerator deleyedStart(){
		yield return new WaitForSeconds (0.5f);
		Application.LoadLevel ("Scene");
	}

	void OnMouseEnter() {
		GetComponent<TextMesh>().fontSize = 42;
	}

	void OnMouseExit() {
		GetComponent<TextMesh>().fontSize = 22;
	}

}