﻿using UnityEngine;
using System.Collections;

public class EnemyAI : MonoBehaviour {

	public Transform m_player;
	public EnemyState currentState;
	public float viewDistance = 100;
	private NavMeshAgent m_agent;
	private bool m_alive = true;
	public ParticleSystem bleedSystem;
	public GameObject explodePrefab;
	
	private bool m_stay = true;
	private bool m_kill = true;
	
	void Start(){
		m_agent = GetComponent<NavMeshAgent> ();
		currentState = new EnemyStateMove(m_player, m_agent);
	}
	
	void Update () {
		if(m_alive){	
			if (currentState.update(transform)){
				currentState = new EnemyStateMove(m_player, m_agent);
			}
			
			checkPlayer();
			killPlayer();
			
		}
	}

	public void checkPlayer(){
		if(Vector3.Distance(m_player.position, transform.position) < viewDistance && transform.GetChild(0).renderer.isVisible){
			overrideState(new EnemyStateRotate(Quaternion.Euler(new Vector3(0,Random.Range (0,360),0)), Random.Range(10,30f)));
			m_agent.Stop();
		}
	}
	
	public void killPlayer(){
		if(Vector3.Distance(m_player.position, transform.position) < 5){
			overrideState(new EnemyStateKill(m_player));
			m_agent.Stop();
		}
	}

	public void overrideState(EnemyState state){
		if(currentState.GetType() != state.GetType()){
			currentState = state;
		}
	}

	public void Dead(){
		m_alive = false;
		StartCoroutine("CoDead");
	}
	
	IEnumerator CoDead(){
		float starttime = Time.time;
		float duration = 10;
		bleedSystem.Play ();
		yield return new WaitForSeconds(1f);
		Instantiate (explodePrefab, transform.position, Quaternion.identity);
		gameObject.SetActive (false);

	}

}

 public class EnemyState{
	public virtual bool update(Transform enemyCube){return false;}
}

 public class EnemyStateMove : EnemyState{
	private Vector3 m_direction;
	private float m_speed = 0.045f;
	private bool m_setDir = true;
	private Transform m_player;
	private float lifeTime = 10;
	private float currentTime;
	
	//robins
	private NavMeshAgent m_agent;
	private Quaternion targetRotation;

	public EnemyStateMove(Transform player, NavMeshAgent agent){
		currentTime = Time.time;
		m_player = player;
		
		m_agent = agent;
		targetRotation = Quaternion.LookRotation (m_player.position - m_agent.transform.position - Vector3.up * 1f);
		m_agent.SetDestination (m_player.position);
	}

	public override bool update (Transform enemyCube){
		if(currentTime + lifeTime < Time.time){
			return true;
		}
		if(m_setDir){
			m_agent.SetDestination (m_player.position);
			m_setDir = false;
		}
		
		float f = 50 * Time.deltaTime;
		enemyCube.rotation = Quaternion.RotateTowards (enemyCube.rotation, targetRotation, f);
		return isAtDestination(m_agent);
	}
		
	bool isAtDestination(NavMeshAgent agent){
		if (!agent.pathPending){
			if (agent.remainingDistance <= agent.stoppingDistance){
				return true;
			}
		}
		return false;
	}
	
}


public class EnemyStatePause : EnemyState{
	private float lifeTime = 5;
	private float currentTime;
	
	public EnemyStatePause(){
		currentTime = Time.time;
	}
	
	public override bool update (Transform enemyCube){
		if(currentTime + lifeTime < Time.time){
			return true;
		}
		return false;
	}
}

public class EnemyStateKill : EnemyState{
	private Transform m_player;
	
	public EnemyStateKill(Transform player){
		m_player = player;
	}
	
	public override bool update (Transform enemyCube){
		enemyCube.position = Vector3.Lerp(enemyCube.position, m_player.position, 0.1f);
		Debug.Log(m_player);
		m_player.GetComponent<Dead>().playerDead(enemyCube);
			//dead
		return true;
	}
}

class EnemyStateRotate : EnemyState{
	public Quaternion targetRotation;
	public float rotationSpeed;
	public EnemyStateRotate(Quaternion targetRot,float speed){
		targetRotation = targetRot;
		rotationSpeed = speed;
	}
	public override bool update(Transform cube){
		float f = rotationSpeed * Time.deltaTime;
		cube.rotation = Quaternion.RotateTowards (cube.rotation, targetRotation, f);
		if (cube.rotation == targetRotation) {
			return true;	
		}
		return false;
	}
}





