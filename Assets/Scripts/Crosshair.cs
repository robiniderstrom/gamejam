﻿using UnityEngine;
using System.Collections;

public class Crosshair : MonoBehaviour {

	public Texture2D m_texture;
	private Rect m_position;
	//public Transform enemy;
	private Transform lastObserved;
	private bool bullet = true;
	public bool enemyWatchKill = false;

	public AudioClip snalldo;

	public ParticleSystem pSystem;

	public AudioClip clip;
	
	void Start(){
		Screen.showCursor = false;
		m_position = new Rect((Screen.width - m_texture.width) / 2, (Screen.height - m_texture.height) /2, m_texture.width, m_texture.height);
	}
	
	void OnGUI(){
		GUI.DrawTexture(m_position, m_texture);
	}
	
	void Update(){
		RaycastHit hit;
		Transform temp = Camera.main.transform;
		//fixa fin variabel till längd
		if(Physics.Raycast(temp.position, temp.forward, out hit)){
			if(Input.GetMouseButtonDown(0) && bullet){
				audio.clip = clip;
				audio.Play();
				pSystem.Play();
				bullet = false;
				Debug.Log(hit.transform.tag);
				if(hit.transform.CompareTag("Normal")){
					AudioSource.PlayClipAtPoint(snalldo,hit.point);
				}
				if(hit.transform.CompareTag("Enemy")){
					Debug.Log("HIT");
					hit.transform.GetComponent<EnemyAI_Second>().Dead();
				}else{
					StartCoroutine(delayedDie());
				
				}
			}else{ //we did not fire this frame but we a re looking at something
				if(hit.transform.CompareTag("Enemy") || hit.transform.CompareTag("Normal")){
					if (lastObserved != hit.transform){ // its another cube
						if(lastObserved != null){ //we have been looing at another one
							lastObserved.SendMessage("ObserveExit");
						}
						lastObserved = hit.transform; //set the new cube
						lastObserved.SendMessage("ObserveEnter"); //tell it we are looking at it
					}
				}else{ //its the walls
				if(lastObserved != null){ 
					lastObserved.SendMessage("ObserveExit");
				}
				lastObserved = null;
			}
			}
		}else{
			if(lastObserved != null){ 
				lastObserved.SendMessage("ObserveExit");
			}
			lastObserved = null;
		}
	}
	
	IEnumerator delayedDie(){
		yield return new WaitForSeconds (2f);
		GameObject.FindGameObjectWithTag("Enemy").GetComponent<EnemyAI_Second>().killPlayer();
		
	}
	
}

