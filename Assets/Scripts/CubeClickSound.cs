﻿using UnityEngine;
using System.Collections;

public class CubeClickSound : MonoBehaviour {

	public AudioClip[] clips;
	private AudioSource source;
	public Vector2 pitchRange = new Vector2(-0.2f,0.2f);
	public Vector2 delayRange = new Vector2(0.1f,0.4f);
	private float delay;
	private float lastTime = 0;

	private Transform target;
	// Use this for initialization
	void Start () {
		target = GameObject.Find ("Player").transform;
		source = GetComponent<AudioSource> ();
		delay = Random.Range (delayRange.x, delayRange.y);

	}
	
	// Update is called once per frame
	void Update () {
		if (Time.time > lastTime + delay) {
			delay = Random.Range (delayRange.x, delayRange.y);
			lastTime = Time.time;
			if((target.position - transform.position).magnitude < 15){
				source.clip = clips[Random.Range(0,clips.Length)];
				source.pitch = 1 + Random.Range(pitchRange.x,pitchRange.y);
				source.Play();
			}
		}
	}
}
