﻿using UnityEngine;
using System.Collections;

public class CubeSpawn : MonoBehaviour {

	public Transform prefab;
	public int spawnNumber = 100;

	public int range = 10;

	private Transform parentObj;

	// Use this for initialization
	void Start () {
		parentObj = new GameObject ().transform;
		parentObj.name = "CubeContainer";
		for (int i = 0; i < spawnNumber; i++) {
			Transform temp = Instantiate(prefab,new Vector3(Random.Range (-range/2,range/2),0,Random.Range (-range/2,range/2)),Quaternion.identity) as Transform;
			temp.parent = parentObj;
		}
	}
	
}
