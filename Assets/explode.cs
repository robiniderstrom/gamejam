﻿using UnityEngine;
using System.Collections;

public class explode : MonoBehaviour {

	// Use this for initialization
	void Start () {
		foreach (Transform t in transform) {
			t.rigidbody.AddExplosionForce(100f, transform.localPosition, 10f, 3.0F);		
		}
	}
	

}
